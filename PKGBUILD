# Maintainer: Antonio Rojas <arojas@archlinux.org>

pkgname=msolve
pkgver=0.7.4
pkgrel=1
pkgdesc='Library for polynomial system solving through algebraic methods'
url='https://msolve.lip6.fr/'
arch=(x86_64)
license=(GPL-2.0-or-later)
depends=(flint
         gcc-libs
         glibc
         gmp)
makedepends=(git)
source=(git+https://github.com/algebraic-solving/msolve#tag=v$pkgver)
sha256sums=('ed832f9acae117993d755960472e5322972b97a73e2450957d6c2a97318e5577')

prepare() {
  cd $pkgname
  sed -e '/AX_EXT/d' -i configure.ac # Disable CPU autodetection
  autoreconf -vi
}

build() {
  cd $pkgname
  ./configure \
    --prefix=/usr
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool # Fix overlinking
  make
}

package() {
  cd $pkgname
  make DESTDIR="$pkgdir" install
}
